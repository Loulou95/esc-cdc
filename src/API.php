<?php
namespace Esc\CDC;

use \GuzzleHttp\Client;


class API {
    const BASE_URL = 'https://cdc.eastsideco.io/api/v1';
    const COLLECTION_ROUTE = '/collect/{project_id}';

    static $client;
    static $projectId;
    static $apiKey;
    static $apiSecret;


    public static function setup($projectId, $apiKey, $apiSecret) {
        static::$projectId = $projectId;
        static::$apiKey = $apiKey;
        static::$apiSecret = $apiSecret;
    }


    public static function sendData($data) {
        if (!static::$apiKey || !static::$apiSecret || !static::$projectId) {
            \Log::error('Esc-CDC: ERROR: Not configured.');
            return false;
        }

        if (!$client) {
            $client = new Client;
        }

        $url = self::BASE_URL.str_replace('{project_id}', static::$projectId, self::COLLECTION_ROUTE);

        $response = $client->request('POST', $url, [
            'auth' => [static::$apiKey, static::$apiSecret],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            \Log::error('Esc-CDC: ERROR: '.$response->getBody());
            return false;
        }

        return true;
    }
}
